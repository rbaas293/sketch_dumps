/**
 * EthernetCommand - A Wiring/Arduino library to tokenize and parse commands
 * received over a serial port.
 * 
 * Version 20180222 
 * Sinjin Jones 
 * Signalysis
 */
#include "EthernetCommand.h"

/**
 * Constructor makes sure some things are set.
 */
EthernetCommand::EthernetCommand()
  : commandList(NULL),
    commandCount(0),
    defaultHandler(NULL),
    term('\n'),           // default terminator for commands, newline character
    last(NULL)
{
  strcpy(delim, ","); // strtok_r needs a null-terminated string
  clearBuffer();
}

/**
 * Adds a "command" and a handler function to the list of available commands.
 * This is used for matching a found token in the buffer, and gives the pointer
 * to the handler function to deal with it.
 */
void EthernetCommand::addCommand(const char *command, void (*function)()) {
  #ifdef ETHERNETCOMMAND_DEBUG
    Serial.print("Adding Ethernet command (");
    Serial.print(commandCount);
    Serial.print("): ");
    Serial.println(command);
  #endif

  commandList = (EthernetCommandCallback *) realloc(commandList, (commandCount + 1) * sizeof(EthernetCommandCallback));
  strncpy(commandList[commandCount].command, command, ETHERNETCOMMAND_MAXCOMMANDLENGTH);
  commandList[commandCount].function = function;
  commandCount++;
}

/**
 * This sets up a handler to be called in the event that the received command string
 * isn't in the list of commands.
 */
void EthernetCommand::setDefaultHandler(void (*function)(const char *)) {
  defaultHandler = function;
}


/**
 * This checks the Ethernet stream for characters, and assembles them into a buffer.
 * When the terminator character (default '\n') is seen, it starts parsing the
 * buffer for a prefix command, and calls handlers setup by addCommand() member
 */
void EthernetCommand::readEthernet() {
  word pos = ether.packetReceive();
  word len = ether.getTcpPayloadLength();
  Serial.print("pos:");
  Serial.println(String(pos));
  Serial.print("length:");
  Serial.println(len);
  while ( pos > 1 ) {
    char* inChar = ( char* ) Ethernet::buffer[3];   // Read single available character, there may be more waiting
    #ifdef ETHERNETCOMMAND_DEBUG
      Serial.print("[");
      Serial.println(inChar);   // Echo back to serial stream
      Serial.print("]");
    #endif

    if (inChar == term) {     // Check for the terminator (default '\r') meaning end of command
      #ifdef ETHERNETCOMMAND_DEBUG
        Serial.print("Received: ");
        //Serial.println(Ethernet::buffer);
      #endif

      char *command = strtok_r(Ethernet::buffer, delim, &last);   // Search for command at start of buffer
      if (command != NULL) {
        boolean matched = false;
        for (int i = 0; i < commandCount; i++) {
          #ifdef ETHERNETCOMMAND_DEBUG
            Serial.print("Comparing [");
            Serial.print(command);
            Serial.print("] to [");
            Serial.print(commandList[i].command);
            Serial.println("]");
          #endif

          // Compare the found command against the list of known commands for a match
          if (strncmp(command, commandList[i].command, ETHERNETCOMMAND_MAXCOMMANDLENGTH) == 0) {
            #ifdef ETHERNETCOMMAND_DEBUG
              Serial.print("Matched Command: ");
              Serial.println(command);
            #endif

            // Execute the stored handler function for the command
			      Serial.print(command);
			      Serial.print(",");
            (*commandList[i].function)();
            matched = true;
            break;
          }
        }
        if (!matched && (defaultHandler != NULL)) {
          (*defaultHandler)(command);
        }
      }
      clearBuffer();
    }
    else if (isprint(inChar)) {     // Only printable characters into the buffer
      if (bufPos < ETHERNETCOMMAND_BUFFER) {
        Ethernet::buffer[bufPos++] = inChar;  // Put character into buffer
        Ethernet::buffer[bufPos] = '\0';      // Null terminate
        Serial.println("Character In Ethernet Buffer");
      } else {
        #ifdef ETHERNETCOMMAND_DEBUG
          Serial.println("Line buffer is full - increase ETHERNETCOMMAND_BUFFER");
        #endif
      }
    } else {
      delay(100);
      word pos = ether.packetReceive();
      word len = ether.getTcpPayloadLength();
      Serial.print("pos:");
      Serial.println(pos);
      Serial.print("length:");
      Serial.println(len);
      Serial.print("Buffer1:");
      Serial.print(String(Ethernet::buffer[0]));
      Serial.print("   Buffer2:");
      Serial.print(String(Ethernet::buffer[2]));
      Serial.print("   Buffer3:");
      Serial.println(String(Ethernet::buffer[3]));
    }
  }
  Serial.println("out of loop");
}
/*
 * New Ethernet Read that takes the UDPdata String as input and sparces it for matching commands
 */
void EthernetCommand::readEthernet2(String RawData){
  int len = ETHERNETCOMMAND_BUFFER;
  char Data[len];
  RawData.toCharArray(Data,len);
  /*
  Serial.print("Length:");
  Serial.print(len);
  Serial.print("    RawData:");
  Serial.println(RawData);
  */
  char *command = strtok_r(Data, delim, &last);   // Search for command at start of buffer
  //char *arg = strtok_r(NULL, delim, &last);
  //Serial.print("Arguement1 is: [");
  //Serial.print(arg);
  //Serial.println("]");
  if (command != NULL) {
    boolean matched = false;
    for (int i = 0; i < commandCount; i++) {
      #ifdef ETHERNETCOMMAND_DEBUG
        Serial.print("Comparing [");
        Serial.print(command);
        Serial.print("] to [");
        Serial.print(commandList[i].command);
        Serial.println("]");
      #endif

      // Compare the found command against the list of known commands for a match
      if (strncmp(command, commandList[i].command, ETHERNETCOMMAND_MAXCOMMANDLENGTH) == 0) {
        #ifdef ETHERNETCOMMAND_DEBUG
          Serial.print("Matched Command: ");
          Serial.println(command);
        #endif

        // Execute the stored handler function for the command
        Serial.print(command);
        Serial.print(",");
        (*commandList[i].function)();
        matched = true;
        break;
      }
    }
    if (!matched && (defaultHandler != NULL)) {
      (*defaultHandler)(command);
    }
  }
  //Serial.println("Buffer Cleared");
  clearBuffer();
}

/*
 * Clear the input buffer.
 */
void EthernetCommand::clearBuffer() {
  Ethernet::buffer[0] = '\0';
  bufPos = 0;
}

/**
 * Retrieve the next token ("word" or "argument") from the command buffer.
 * Returns NULL if no more tokens exist.
 */
char *EthernetCommand::next() {
  return strtok_r(NULL, delim, &last);
}

