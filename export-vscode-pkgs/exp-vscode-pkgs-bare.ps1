# Print packages one per line with no formatting to the specified directory.
$outdir = $env:pwd # Default is pwd.
# Only list installed packages, no themes [hence adding and "ip" to the filename.
$outname = "atom-pkgs-" + $env:USERNAME + "_i" + "p" + ".txt"
# Command:
apm list --installed --packages --bare > $outname