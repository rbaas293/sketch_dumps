/**
 * EthernetCommand - A Wiring/Arduino library to tokenize and parse commands
 * received over a serial port.
 * 
 * Version 20180222 
 * Sinjin Jones 
 * Signalysis
 */
#ifndef EthernetCommand_h
#define EthernetCommand_h

#if defined(WIRING) && WIRING >= 100
  #include <Wiring.h>
#elif defined(ARDUINO) && ARDUINO >= 100
  #include <Arduino.h>
#else
  #include <WProgram.h>
#endif
#include <string.h>
#include <EtherCard.h>

// Size of the input buffer in bytes (maximum length of one command plus arguments)
#define ETHERNETCOMMAND_BUFFER 48
// Maximum length of a command excluding the terminating null
#define ETHERNETCOMMAND_MAXCOMMANDLENGTH 6

// Uncomment the next line to run the library in debug mode (verbose messages)
#define ETHERNETCOMMAND_DEBUG   ///////////////////////////////////////////////////////////////////////////////// Changed to run in Debug Mode


class EthernetCommand {
  public:
    EthernetCommand();      // Constructor
    void addCommand(const char *command, void(*function)());  // Add a command to the processing dictionary.
    void setDefaultHandler(void (*function)(const char *));   // A handler to call when no valid command received.

    void readEthernet();    // Main entry point.
    void readEthernet2(String RawData);
    void clearBuffer();   // Clears the input buffer.
    char *next();         // Returns pointer to next token found in command buffer (for getting arguments to commands).

  private:
    // Command/handler dictionary
    struct EthernetCommandCallback {
      char command[ETHERNETCOMMAND_MAXCOMMANDLENGTH + 1];
      void (*function)();
    };                                    // Data structure to hold Command/Handler function key-value pairs
    EthernetCommandCallback *commandList;   // Actual definition for command/handler array
    byte commandCount;

    // Pointer to the default handler function
    void (*defaultHandler)(const char *);

    char delim[2]; // null-terminated list of character to be used as delimeters for tokenizing (default ",")
    char term;     // Character that signals end of command (default '\n')

    char buffer[ETHERNETCOMMAND_BUFFER + 1]; // Buffer of stored characters while waiting for terminator character
    byte bufPos;                        // Current position in the buffer
    char *last;                         // State variable used by strtok_r during processing
};

#endif //EthernetCommand_h
