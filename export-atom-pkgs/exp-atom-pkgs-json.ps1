# Export atoms packages in json format to the specified directory.
$outdir = $env:pwd # Default is pwd.
$outname = "atom-pkgs-" + $env:USERNAME + "_i" + "p" + ".json"
# Command
apm list --installed --packages --json > $outname 