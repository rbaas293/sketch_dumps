# Sketch Dumps by Ryan Baas

* Purpose of this repository is to house all my unorganized bullshit in one place until I can organize it. So different folders will each be separate projects and stuff.
* I will use this `README.md` as _note space_. Mostly for links or little tips or instructions that are not easily taken down elsewhere. 

#### To play with my dumps.... :
* Cone this repository:
```git
git clone https://gitlab.com/rbaas293/sketch_dumps.git
```

### =====================%%Start Note Section%%====================
### ============================================================= 

### Current Atom Package List: (On `baas-razer`):

>  c



#### PS Script to export my atom packages.

* Options Available:
```Shell
Options for [apm list <options>]:
  --disabled       Print only disabled packages                                            [boolean]
  -b, --bare       Print packages one per line with no formatting                          [boolean]
  -e, --enabled    Print only enabled packages                                             [boolean]
  -d, --dev        Include dev packages                                    [boolean] [default: true]
  -h, --help       Print this usage message
  -i, --installed  Only list installed packages/themes                                     [boolean]
  -j, --json       Output all packages as a JSON object                                    [boolean]
  -l, --links      Include linked packages                                 [boolean] [default: true]
  -t, --themes     Only list themes                                                        [boolean]
  -p, --packages   Only list packages                                                      [boolean]
```
* Json Script:
```PowerShell
# Export atoms packages in json format to the specified directory.
$outdir = $env:pwd # Default is pwd.
$outname = "atom-pkgs-" + $env:USERNAME + "i" + ".json"
# Command
apm list --installed --json > $outname 
```
* Bare Text Script:
```PowerShell
# Print packages one per line with no formatting to the specified directory.
$outdir = $env:pwd # Default is pwd.
# Only list installed packages, no themes [hence adding and "ip" to the filename.
$outname = "atom-pkgs-" + $env:USERNAME + "_i" + "p" + ".txt"
# Command:
apm list --installed --packages --bare > $outname 
```

### If `/boot` partition or directory is full:
* One-liner:
```cmd
dpkg --get-selections|grep 'linux-image*'|awk '{print $1}'|egrep -v "linux-image-$(uname -r)|linux-image-generic" |while read n;do apt-get -y remove $n;done
```

* Script:
```cmd
dpkg --get-selections | \
  grep 'linux-image*' | \
  awk '{print $1}' | \
  egrep -v "linux-image-$(uname -r)|linux-image-generic" | \
  while read n
  do
    apt-get -y remove $n
  done
```
### How do I free up more space in / boot
You've a lot unused kernels. Remove all but the last kernels with:

    sudo apt-get purge linux-image-{3.0.0-12,2.6.3{1-21,2-25,8-{1[012],8}}}
This is shorthand for:

    sudo apt-get purge linux-image-3.0.0-12 linux-image-2.6.31-21 linux-image-2.6.32-25 linux-image-2.6.38-10 linux-image-2.6.38-11 linux-image-2.6.38-12 linux-image-2.6.38-8
Removing the `linux-image-x.x.x-x` package will also remove `linux-image-x.x.x-x-generic`.

The headers are installed into `/usr/src` and are used when building out-tree kernel modules (like the proprietary nvidia driver and virtualbox). Most users should remove these header packages if the matching kernel package (`linux-image-*`) is not installed.

To list all installed kernels, run:

    dpkg -l linux-image-\* | grep ^ii
One command to show all kernels and headers that can be removed, excluding the current running kernel:

    kernelver=$(uname -r | sed -r 's/-[a-z]+//')
    dpkg -l linux-{image,headers}-"[0-9]*" | awk '/ii/{print $2}' | grep -ve $kernelver
It selects all packages named starting with `linux-headers-<some number>` or `linux-image-<some number>`, prints the package names for installed packages and then excludes the current loaded/running kernel (not necessarily the latest kernel!). This fits in the recommendation of testing a newer kernel before removing older, known-to-work kernels.

So, after upgrading kernels and rebooting to test it, you can remove all other kernels with:

    sudo apt-get purge $(dpkg -l linux-{image,headers}-"[0-9]*" | awk '/ii/{print $2}' | grep -ve "$(uname -r | sed -r 's/-[a-z]+//')")
    
--Lekensteyn

@ <http://askubuntu.com/questions/89710/ddg#90219>. Just putting it here for my ease of refrence. Thanks!