```PowerShell
code --list-extensions
    
    felipecaputo.git-project-manager
    ms-python.python
    ms-vscode.cpptools
    ms-vscode.PowerShell
    patnym.r2g
    platformio.platformio-ide
    Ray.auto-git
    shd101wyy.markdown-preview-enhanced
    teledemic.branch-warnings
    telesoho.vscode-markdown-paste-image
    vsciot-vscode.vscode-arduino
